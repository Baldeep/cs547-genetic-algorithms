### What is this repository for? ###


This repository is for assignment 1 of CS547 - Advanced Topics in Software Engineering. 


The assignment consists of creating a Genetic Algorithm which evolves a population of random strings into the string "Hello World!"


The main aspects to implement are:
Population initialisation, Selection, Crossover, Mutation, Termination


Performance also needs to be compared to Hill climb and random selection Algorithms to achieve the same result. 


[MyPlace link](http://classes.myplace.strath.ac.uk/mod/assign/view.php?id=774019)

### How do I get set up? ###

All code is started in the Main class.