
import java.util.ArrayList;
import java.util.Random;

public class FittestSelector {

	private Random r = new Random();
	
	/**
	 * Selects a subsect of the given population. 
	 * 
	 * Will select a percentage of the fittest individuals determined by {@linkplain Parameters#FITTEST_PERCENTAGE}
	 * in the population and then add a percentage of "unfit" individuals from the remainder of the population
	 * @param population
	 * @return
	 */
	public ArrayList<Individual> select(ArrayList<Individual> population) {
		ArrayList<Individual> selection = new ArrayList<Individual>();
		
		int fittestSize = population.size() * Parameters.FITTEST_PERCENTAGE /100;
		
		if(fittestSize > population.size()){
			fittestSize = population.size();
		}
		 
		// Add a selected percentage of the fittest individuals
		for(int i = 0; i < fittestSize; i++){
			selection.add(population.get(i));
		}
		
		// Mix in a selected percentage of "unfit" individuals from the remaining population
		int unfitSize = population.size() * (Parameters.UNFIT_PERCENTAGE / 100);
		
		for(int i = 0; i < unfitSize; i++){
			selection.add(
					population.get(r.nextInt((population.size()-fittestSize)) + fittestSize));
		}
		
		return selection;
	}

}
