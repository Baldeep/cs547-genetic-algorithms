import java.util.ArrayList;
import java.util.Collections;

public class HillClimber {

	public Individual hillClimb(String target){
		RandomStringGenerator rndstrgen = new RandomStringGenerator();
		FitnessFunction ff = new FitnessFunction(target);

		Individual bestIndividual = new Individual(rndstrgen.getRandomString(target.length()));
		bestIndividual.setScore(ff.evaluateIndividual(bestIndividual));


		int iterations = 0;


		for(int j = 0; j < Parameters.RESET_HILL; j++){
			boolean finished = false;
			while(!finished){
				String str;
				if(j == 0){
					str = bestIndividual.getString();
				} else {
					str = rndstrgen.getRandomString(target.length());
				}

				ArrayList<String> neighbourStrings = getNeighbours(str);
				ArrayList<Individual> neighbours = new ArrayList<Individual>();
				for(int i = 0; i < neighbourStrings.size(); i++){
					Individual individual = new Individual(neighbourStrings.get(i));
					individual.setScore(ff.evaluateIndividual(individual));
					neighbours.add(individual);
					
				}

				Collections.sort(neighbours, new IndividualComparator());

				if(neighbours.size() == 0){
					finished = true;
				} else if(neighbours.get(0).getScore() > bestIndividual.getScore()){
					finished = true;
				} else {
					if(Parameters.GA_DEBUG)
						System.out.println(bestIndividual.getString());
					bestIndividual = neighbours.get(0);
				}

				iterations++;
			}
			
			if(bestIndividual.getScore() == 0){
				break;
			}
		}

		System.out.println("Best match to \"" + target + "\" found is: \"" + bestIndividual.getString() + "\".");
		System.out.println("Found in " + iterations + " iterations, ");

		return bestIndividual;
	}

	private ArrayList<String> getNeighbours(String string) {
		char[] str = string.toCharArray();

		ArrayList<String> neighbours = new ArrayList<>();

		for(int i = 0; i < str.length; i++){
			str[i] += 1;
			neighbours.add(new String(str));
			str[i] -= 2;
			neighbours.add(new String(str));	
			str[i] += 1;
		}

		return neighbours;
	}
}
