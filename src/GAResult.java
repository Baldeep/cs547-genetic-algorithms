

public class GAResult {
	
	private int generations;
	
	private long timeTaken;
	
	private String stringFound;
	
	private boolean finished;
	
	private boolean plateau;
	
	private boolean generationLimit;

	public GAResult(int generations, String stringFound, boolean finished, boolean plateau,
			boolean generationLimit) {
		this.generations = generations;
		this.stringFound = stringFound;
		this.finished = finished;
		this.plateau = plateau;
		this.generationLimit = generationLimit;
	}

	/**
	 * @return the generations
	 */
	public int getGenerations() {
		return generations;
	}

	/**
	 * @param generations the generations to set
	 */
	public void setGenerations(int generations) {
		this.generations = generations;
	}

	/**
	 * @return the timeTaken
	 */
	public long getTimeTaken() {
		return timeTaken;
	}

	/**
	 * @param timeTaken the timeTaken to set
	 */
	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}

	/**
	 * @return the stringFound
	 */
	public String getStringFound() {
		return stringFound;
	}

	/**
	 * @param stringFound the stringFound to set
	 */
	public void setStringFound(String stringFound) {
		this.stringFound = stringFound;
	}

	/**
	 * @return the finished
	 */
	public boolean isFinished() {
		return finished;
	}

	/**
	 * @param finished the finished to set
	 */
	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	/**
	 * @return the plateau
	 */
	public boolean isPlateau() {
		return plateau;
	}

	/**
	 * @param plateau the plateau to set
	 */
	public void setPlateau(boolean plateau) {
		this.plateau = plateau;
	}

	/**
	 * @return the generationLimit
	 */
	public boolean isGenerationLimit() {
		return generationLimit;
	}

	/**
	 * @param generationLimit the generationLimit to set
	 */
	public void setGenerationLimit(boolean generationLimit) {
		this.generationLimit = generationLimit;
	}
	
	public void prettyPrint(){
		System.out.println("Best match to \"" + Parameters.TARGET_STR + "\" found is: \"" + stringFound +  "\".");
		System.out.println("Found in " + generations + " generations.");
		if(plateau){
			System.out.println("Results plateaued over the course of " + Parameters.PLATEAU_SIZE + " generations.");
		} 
		if(generationLimit){
			System.out.println("Generation limit of " + Parameters.GENERATIONS_LIMIT + " reached.");
		}
	}
	
}
