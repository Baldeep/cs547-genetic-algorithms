
import java.util.ArrayList;
import java.util.Collections;

public class GeneticAlgorithm {

	/**
	 * Uses a genetic algorithm to reach the target string from a population of random strings. 
	 * @param target - The target string to find.
	 * @return Returns true if the string was found successfully, false if the algorithm was stopped due to other circumstances.
	 */
	public GAResult runAlgorithm(String target){
		RandomStringGenerator rndstr = new RandomStringGenerator();
		FitnessFunction evaluator = new FitnessFunction(target);

		ArrayList<Individual> population = new ArrayList<Individual>();

		// Initialize starting population.
		for(int i = 0; i < Parameters.POPULATION_SIZE; i++){
			population.add(new Individual(rndstr.getRandomString(target.length())));
		}

		// Initial population created, start loop

		ArrayList<Integer> scores = new ArrayList<Integer>();
		int generation = 0;

		boolean goToNextGen = true;
		boolean plateaued = false;

		while(goToNextGen){

			// Evaluate members
			for(int i = 0; i < population.size(); i++){
				Individual individual = population.get(i);
				individual.setScore(evaluator.evaluateIndividual(individual));
				population.set(i, individual);
			}

			// Sort population and select candidates
			Collections.sort(population, new IndividualComparator());

			if(Parameters.GA_DEBUG){
				System.out.println("Generation: " + generation + 
						", best score: " + population.get(0).getScore() + 
						" : " + population.get(0).getString());
			}

			if(population.get(0).getString().equals(Parameters.TARGET_STR)){
				break;
			}

			// save the lowest score 
			scores.add(population.get(0).getScore());

			// If the scores have levelled out, stop
			if(stopping(scores)){
				System.out.println("Results have levelled off, stopping.");
				plateaued = true;
				break;
			}

			// Select population for breeding.
			ArrayList<Individual> selectedPopulation = new FittestSelector().select(population);

			// Generate next generation.
			population = new Crossover().crossover(selectedPopulation);

			generation ++;

			if(Parameters.GENERATIONS_LIMIT >= 0 && generation == Parameters.GENERATIONS_LIMIT){
				System.out.println("Generations limit reached, stopping.");
				goToNextGen = false;
			}
		}

		Collections.sort(population, new IndividualComparator());
		System.out.println("Best match to \"" + Parameters.TARGET_STR + "\" found is: \"" + population.get(0).getString()+ "\".");
		System.out.println("Found in " + generation + " generations.");
		if(plateaued){
			System.out.println("Results plateaued over the course of " + Parameters.PLATEAU_SIZE + " generations.");
		} 
		if(!goToNextGen){
			System.out.println("Generation limit of " + Parameters.GENERATIONS_LIMIT + " reached.");
		}

		return new GAResult(generation, population.get(0).getString(), !(plateaued || !goToNextGen), plateaued, !goToNextGen);
	}

	/**
	 * Checks if the scores are levelling out and no better solution is likely. 
	 * @param scores An array containing a "history" of the lowest score found in each generation.
	 * @return Returns true if 
	 */
	private boolean stopping(ArrayList<Integer> scores){
		if(scores.size() < Parameters.PLATEAU_SIZE){
			return false;
		}/*
		System.out.print("\n");
		for(int i = 0; i < scores.size(); i++){
			System.out.print(scores.get(i) + ",");
		}
		System.out.print("\n");
		 */
		boolean stop = true;

		int current = scores.get(scores.size()-1);

		for(int i = scores.size()-2; i >= (scores.size() - Parameters.PLATEAU_SIZE ); i--){
			int difference = current - scores.get(i);
			if(difference < 0 || difference > Parameters.PLATEAU_RANGE){
				stop = false;
			}
		}

		return stop;
	}
}
