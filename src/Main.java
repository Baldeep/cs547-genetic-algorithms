

public class Main {

	public static void main(String[] args){

		geneticAlgorithm(1);
		System.out.println("\n----\n");
		
		randomSearch();
		System.out.println("\n----\n");
		
		hillClimber();
	}
	
	private static void hillClimber(){
		HillClimber hc = new HillClimber();
		System.out.println("Hill Climb");
		long startTime = System.currentTimeMillis();
		hc.hillClimb(Parameters.TARGET_STR);
		long endTime = System.currentTimeMillis();
		
		long totalTime = endTime - startTime;
		System.out.println("Hill Climb");
		System.out.println("Time Taken: " + formatTime(totalTime));
	}
	
	private static void randomSearch() {
		RandomSearch rs = new RandomSearch();
		
		long startTime = System.currentTimeMillis();
		Individual i = rs.randomSearch(Parameters.TARGET_STR);
		long endTime = System.currentTimeMillis();
		
		long totalTime = endTime - startTime;
		System.out.println("Random Search \nBest match to \"" + Parameters.TARGET_STR 
					+ "\" found is: \"" + i.getString() 
					+ "\".\nFound in " + Parameters.ITERATIONS_LIMIT + " iterations.");
		System.out.println("Time Taken: " + formatTime(totalTime));
	}

	private static void geneticAlgorithm(int loops){
		GeneticAlgorithm ga = new GeneticAlgorithm();

		int successes = 0;
		int generationTotal = 0;
		long totalTimeLoops = 0;
		GAResult mostGens = null;
		GAResult leastGens = null;
		System.out.println("Genetic Algorithm");
		
		for(int i = 0; i < loops; i++){
			if(loops>1)
				System.out.println("Loop " + (i+1));
			
			long startTime = System.currentTimeMillis();
			GAResult r = ga.runAlgorithm(Parameters.TARGET_STR);
			long endTime = System.currentTimeMillis();

			if(r.isFinished()){
				successes++;
			}
			
			generationTotal += r.getGenerations();
			
			if(mostGens == null || mostGens.getGenerations() < r.getGenerations()){
				mostGens = r;
			}
			
			if(leastGens == null || leastGens.getGenerations() > r.getGenerations()){
				leastGens = r;
			}
			
			long totalTime = endTime - startTime;
			r.setTimeTaken(totalTime);
			totalTimeLoops += totalTime;
			
			System.out.println("Time Taken: " + formatTime(totalTime));
			
		}
		if(loops>1){
			System.out.println("\nFound target " + successes + " out of " + loops + " times.");
			
			System.out.println("Average Generations: " + generationTotal/loops);
			System.out.println("Average Time Taken: " + formatTime(totalTimeLoops));
			System.out.println("Least Generations: ");
			leastGens.prettyPrint();
			System.out.println("MostGenerations: ");
			mostGens.prettyPrint();
		}
	}
	
	private static String formatTime(long time){
		long mins = (time / 1000) /60;
		long secs = (time / 1000) % 60;
		long mils = (time %1000);
		return new String(mins + " m " + secs + " s " + mils + " ms");
		
	}

}
