
import java.util.Random;

public class RandomStringGenerator {

	private Random r = new Random();
	/**
	 * Generates a string of a size specified by {@linkplain Parameters#STRING_LEN}. 
	 * @return Returns a string of random ASCII characters. 
	 */
	
	public String getRandomString(int maxStrLen){

		String individual = "";

		int strlen = maxStrLen; //r.nextInt(maxStrLen);
		for(int i = 0; i < strlen; i ++){
			// ASCII encodes using 8 bit, the first 32 chars are control characters.
			int charAsInt = r.nextInt(128 - 32) + 32;
			char c = (char) charAsInt;
			individual += c;
		}
		
		return individual;
	}
}
