public class FitnessFunction {

	String target;
	
	public FitnessFunction(String target){
		this.target = target;
	}
	
	public int evaluateIndividual(Individual individual){
		char[] stringAsChars = individual.getStringArray();
		char[] targetAsChars = target.toCharArray();
		
		int score = 0;
		
		// If the strings are not the same length, add a penalty for mismatched length
		if(stringAsChars.length != targetAsChars.length){
			score += Parameters.LENGHT_PENALTY;
		}
		
		// If the strings are uneven length, pick the shortest
		int limit = Math.min(stringAsChars.length, targetAsChars.length);
		
		// Now calculate the distance
		for(int i = 0; i < limit; i ++){
			score += calculateDistance(targetAsChars[i], stringAsChars[i]);
		}
		
		return score;
	}
	
	private int calculateDistance(char firstChar, char secondChar){
		int distance = 0;
		
		int first = (int) firstChar;
		int second = (int) secondChar;

		distance = first - second;
	
		return Math.abs(distance);
	}
	
}
