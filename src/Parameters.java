
public class Parameters {

	/**
	 * The target String to get to.
	 */
	public static final String TARGET_STR = "Hello World!";
	
	/**
	 * Random Search iterations limit
	 */
	public static final int ITERATIONS_LIMIT = 100000;
	
	/**
	 * The times the hill climber is reset each time it finds a maximum, in order to stop it getting stuck on local maxima. 
	 */
	public static final int RESET_HILL = 50;
	
	/**
	 * The maximum size of the population. 
	 */
	public static final Integer POPULATION_SIZE = 100;

	/**
	 * The percentage (max 100) of the population to take to breed in order of fitness when selecting by fitness. 
	 * 
	 * {@linkplain Parameters#FITTEST_PERCENTAGE} + {@linkplain}
	 * 
	 */
	public static final int FITTEST_PERCENTAGE = 40;

	/**
	 * The number of "unfit" individuals to select for breeding, in terms of percentage of population. 
	 * 
	 */
	public static final int UNFIT_PERCENTAGE = 10;

	/**
	 * The maximum number of generations to continue for, set to -1 to run forever
	 */
	public static int GENERATIONS_LIMIT = -1;

	/**
	 * The probability of cross over happening when a pair of individuals is selected from the population
	 */
	public static final double CROSSOVER_PROBABLILITY = 0.7;


	/**
	 * The probability with which the children of a cross over will have a random mutation
	 */
	public static final double MUTATION_PROBABLILITY = 0.1;

	/**
	 * Whether the mutations should be random characters (true) or one hamming distance from the mutated character (false).
	 */
	public static final boolean RANDOM_MUTATION = false;

	/**
	 * The number of generations to look back when checking if the lowest score is levelling off.
	 * 
	 * Must be at least 2. 
	 */
	public static final int PLATEAU_SIZE = 30;

	/**
	 * The range +/- which defines "levelling off" in the scores. 
	 */
	public static final int PLATEAU_RANGE = 3;


	/**
	 * Whether to print if a cross over happened or not each time a new individual is added to the next generation
	 */
	public static final boolean SHOW_CROSSOVER_DEBUG = false;


	/**
	 * The penalty to the score for a mis match in string length.
	 */
	public static final Integer LENGHT_PENALTY = 50;

	public static final boolean GA_DEBUG = false;

	




}
