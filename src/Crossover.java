
import java.util.ArrayList;
import java.util.Random;

public class Crossover {

	private Random r = new Random();
	
	public ArrayList<Individual> crossover(ArrayList<Individual> selectedPopulation) {
		ArrayList<Individual> population = new ArrayList<Individual>();

		// If the population size is an odd number, the new population will be one bigger than the specified size
		while(population.size() < Parameters.POPULATION_SIZE){
			

			char[] firstIndividual = selectedPopulation.get(r.nextInt(selectedPopulation.size())).getStringArray();
			char[] secondIndividual = selectedPopulation.get(r.nextInt(selectedPopulation.size())).getStringArray();

			double crossOverGuess = r.nextDouble();

			// The cross over value is bigger than the probability of cross over happening, 
			// No cross over happens
			if(crossOverGuess > Parameters.CROSSOVER_PROBABLILITY){
				population.add(new Individual(new String(firstIndividual)));
				population.add(new Individual(new String(secondIndividual)));
				
				if(Parameters.SHOW_CROSSOVER_DEBUG)
					System.out.println("Not crossed over");
				
			} else {
				// Other wise, cross over. 
				int cut = r.nextInt(firstIndividual.length);
				
				String firstChild = String.valueOf(firstIndividual).substring(0, cut) + String.valueOf(secondIndividual).substring(cut);
				String secondChild = String.valueOf(secondIndividual).substring(0, cut) + String.valueOf(firstIndividual).substring(cut);
				
				double mutationGuess = r.nextDouble();
				
				if(mutationGuess < Parameters.MUTATION_PROBABLILITY){
					if(Parameters.RANDOM_MUTATION){
						firstChild = randomMutation(firstChild);
						secondChild = randomMutation(secondChild);
					} else {
						firstChild = hammingMutation(firstChild);
						secondChild = hammingMutation(secondChild);
					}
				}
				
				
				population.add(new Individual(firstChild));
				population.add(new Individual(secondChild));
				
				if(Parameters.SHOW_CROSSOVER_DEBUG)
					System.out.println("Crossed over");
			}

		}
		return population;
	}

	private String randomMutation(String child) {
		char[] childArray = child.toCharArray();
		
		int mutationIndex = r.nextInt(child.length());
		
		// ASCII encodes using 8 bit, the first 32 chars are control characters.
		childArray[mutationIndex] = (char) (r.nextInt(128 - 32) + 32);
		
		return new String(childArray);
	}
	
	private String hammingMutation(String child){
		char[] childArray = child.toCharArray();
		
		int mutationIndex = r.nextInt(child.length());
		
		// ASCII encodes using 8 bit, the first 32 chars are control characters.
		int character = (int) childArray[mutationIndex];
		
		boolean coin = r.nextBoolean();
		
		if(coin){
			character = character + 1;
			if(character > 127)
				character = 32; // roll over character
		} else {
			character = character - 1;
			if(character < 32)
				character = 127; // roll over character
		}
		
		childArray[mutationIndex] = (char) character;
		
		return new String(childArray);
	}
}
