

public class RandomSearch {
	
	public Individual randomSearch(String target){
		
		RandomStringGenerator rndstrgen = new RandomStringGenerator();
		FitnessFunction ff = new FitnessFunction(target);
		
		Individual bestIndividual = new Individual(rndstrgen.getRandomString(target.length()));
		bestIndividual.setScore(ff.evaluateIndividual(bestIndividual));
		
		// bestIndividual is the first "iteration"
		for(int i = 1; i < Parameters.ITERATIONS_LIMIT; i++){
			Individual currentIndividual = new Individual(rndstrgen.getRandomString(target.length()));
			currentIndividual.setScore(ff.evaluateIndividual(currentIndividual));
			
			if(currentIndividual.getScore() < bestIndividual.getScore()){
				bestIndividual = currentIndividual;
			}
		}
		
		return bestIndividual;
		
	}
}
