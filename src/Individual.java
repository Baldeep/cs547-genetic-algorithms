

public class Individual {

	private String string;
	private char[] stringArray;
	private int score;
	
	public Individual(String string) {
		this.string = string;
		this.stringArray = string.toCharArray();
	}
	
	public Individual(String string, int score) {
		this.string = string;
		this.stringArray = string.toCharArray();
		this.score = score;
	}
	
	public void printMe(){
		System.out.println(string + ", score: " + score);
	}

	/**
	 * @return the string
	 */
	public String getString() {
		return string;
	}

	/**
	 * @param string the string to set
	 */
	public void setString(String string) {
		this.string = string;
	}
	
	/**
	 * @return the string as an array
	 */
	public char[] getStringArray(){
		return stringArray;
	}
	
	/**
	 * @param stringArray the string as an array
	 */
	public void setStringArray(char[] stringArray){
		this.stringArray = stringArray;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}
	
	
}
