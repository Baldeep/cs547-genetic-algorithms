
import java.util.Comparator;

public class IndividualComparator implements Comparator<Individual>{

	@Override
	public int compare(Individual ind1, Individual ind2) {
		if(ind1.getScore() < ind2.getScore()){
			return -1;
		} else if(ind1.getScore() > ind2.getScore()){
			return 1;
		} else if(ind1.getScore() == ind2.getScore()){
			return 0;
		}
		return -2;
	}
	
	
}
